#ifndef DISPLAY_HAL_H_INCLUDED
#define DISPLAY_HAL_H_INCLUDED

#include "displayhal.adaptor.h"

class DisplayHal
: public org::pragmatux::DisplayHal1_adaptor
, public org::pragmatux::DisplayHal::hdseriesx1_adaptor
, public DBus::ObjectAdaptor
, public DBus::IntrospectableAdaptor
, public DBus::PropertiesAdaptor
{
  public:
	DisplayHal(DBus::Connection &c);

  private:
	void CustomerIdSet(const std::string&);
	std::string CustomerIdGet();
	void UsbReset();
	uint32_t BacklightHoursGet();
};

#endif // DISPLAY_HAL_H_INCLUDED
