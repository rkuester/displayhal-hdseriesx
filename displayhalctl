#!/bin/sh

exe=$(basename $0)
usage="\
usage: $exe customer_id
            - show the customer id
       $exe customer_id STRING
            - set the customer id to STRING
       $exe firmware_info
            - show the firmware info
       $exe bootloader_info
            - show the bootloader info
       $exe backlight_hours
            - show total hours the backlight has been on
       $exe usb_reset
            - reset the hardware connected via USB
       $exe output NR VALUE
            - set the digital output NR the VALUE 0 or 1"

cmd=${1:-help}
busname=org.pragmatux.DisplayHal
method="dbus-send --system --dest=$busname --print-reply=literal"

case $cmd,${2:-} in
	customer_id,)
		$method /org/pragmatux/DisplayHal \
			org.pragmatux.DisplayHal.hdseriesx1.CustomerIdGet |
			sed -e 's/^ *//' -e 's/$/\n/'
		;;

	customer_id,?*)
		$method /org/pragmatux/DisplayHal \
			org.pragmatux.DisplayHal.hdseriesx1.CustomerIdSet \
			string:$2
		;;

	firmware_info,)
		$method /org/pragmatux/DisplayHal \
			org.freedesktop.DBus.Properties.Get \
			string:org.pragmatux.DisplayHal1 \
			string:FirmwareInfo |
			tr --squeeze-repeats [:space:] | cut -f 3 -d' '
		;;

	bootloader_info,)
		$method /org/pragmatux/DisplayHal \
			org.freedesktop.DBus.Properties.Get \
			string:org.pragmatux.DisplayHal1 \
			string:BootloaderInfo |
			tr --squeeze-repeats [:space:] | cut -f 3 -d' '
		;;

	usb_reset,)
		$method /org/pragmatux/DisplayHal \
			org.pragmatux.DisplayHal.hdseriesx1.UsbReset
		;;

	backlight_hours,)
		$method /org/pragmatux/DisplayHal \
			org.pragmatux.DisplayHal.hdseriesx1.BacklightHoursGet |
			tr --squeeze-repeats [:space:] | cut -f 3 -d' '
		;;

	output,?*)
		$method /org/pragmatux/DisplayHal/DigitalOutput$2 \
			org.freedesktop.DBus.Properties.Set \
			string:org.pragmatux.DisplayHal.DigitalOutput1 \
			string:Value \
			variant:uint32:$3
		;;

	help|*)
		echo "$usage"
		;;
esac
