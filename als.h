#ifndef ALS_H_INCLUDED
#define ALS_H_INCLUDED

#include "als.adaptor.h"

class AmbientLightSensor
: public org::pragmatux::DisplayHal::AmbientLightSensor1_adaptor
, public DBus::ObjectAdaptor
, public DBus::IntrospectableAdaptor
, public DBus::PropertiesAdaptor
{
  public:
	AmbientLightSensor(DBus::Connection &c);

  private:
	uint32_t GetIlluminanceRaw();
};

#endif // ALS_H_INCLUDED
