#ifndef KEYSWITCH_H_INCLUDED
#define KEYSWITCH_H_INCLUDED

#include "keyswitch.adaptor.h"

class Keyswitch
: public org::pragmatux::DisplayHal::Keyswitch1_adaptor
, public DBus::ObjectAdaptor
, public DBus::IntrospectableAdaptor
{
  public:
	Keyswitch(DBus::Connection &c, DBus::DefaultMainLoop *loop);
	bool State();

  private:
	int fd;
	DBus::DefaultWatch watch;
	void read_input(DBus::DefaultWatch&);
};

#endif // KEYSWITCH_H_INCLUDED
