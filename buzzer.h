#ifndef BUZZER_H_INCLUDED
#define BUZZER_H_INCLUDED

#include "buzzer.adaptor.h"

class Buzzer
: public org::pragmatux::DisplayHal::Buzzer1_adaptor
, public DBus::ObjectAdaptor
, public DBus::IntrospectableAdaptor
{
  public:
	Buzzer(DBus::Connection &c);

  private:
	void On();
	void Off();
};

#endif // BUZZER_H_INCLUDED
