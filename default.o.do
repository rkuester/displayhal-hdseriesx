: ${CPP:=g++ -std=c++11}
CPPFLAGS="$CPPFLAGS $(pkg-config --cflags dbus-c++-1)"

# Generate D-Bus adaptor or proxy headers if included
$CPP $CPPFLAGS -MM -MG $2.cpp \
	| tr " " "\n" \
	| egrep '.*(adaptor|proxy).h' \
	| xargs -r redo-ifchange

$CPP $CPPFLAGS -c $2.cpp -o $3 
$CPP $CPPFLAGS -MM $2.cpp | read DEPS
redo-ifchange $2.cpp ${DEPS#*:}
