#include <fstream>
#include <string>
#include <sstream>
#include "displayhal.h"

#define attr_path(tail) ("/sys/devices/platform/scom.0/" tail)

template <typename T>
T read_attr(const std::string &path)
{
	std::ifstream attr(path);
	attr.exceptions(std::ifstream::badbit);
	T result;

	try {
		attr >> result;
	}
	catch (...) {
		std::stringstream msg;
		msg << "reading SCOM driver attribute " << path;
		throw DBus::ErrorIOError(msg.str().c_str());
	}

	return result;
}

std::string normalize_model(const std::string &model)
{
	if (model.compare(0, 7, "HD07T21") == 0) {
		return "hdseriesx-07T21-1";
	} else if (model.compare(0, 7, "HD13T21") == 0) {
		return "hdseriesx-13T21-1";
	} else {
		return "unknown";
	}
}

DisplayHal::DisplayHal(DBus::Connection &c)
: DBus::ObjectAdaptor(c, "/org/pragmatux/DisplayHal")
{
	PlatformIdRaw = read_attr<std::string>(attr_path("model"));
	PlatformId = normalize_model(PlatformIdRaw());
	DeviceSerial = read_attr<std::string>(attr_path("serial"));
	FirmwareInfo = read_attr<std::string>(attr_path("main_sw_version"));
	BootloaderInfo = read_attr<std::string>(attr_path("bios_version"));
}

std::string DisplayHal::CustomerIdGet()
{
	return read_attr<std::string>(attr_path("customer_id"));
}

void DisplayHal::CustomerIdSet(const std::string &id)
{
	std::ofstream attr(attr_path("customer_id"));
	attr << id;
	attr.flush();

	if (!attr) {
		throw DBus::ErrorIOError("writing SCOM driver attribute");
	}
}

void DisplayHal::UsbReset()
{
	std::ofstream attr(attr_path("reset_usb"));
	attr << "1";
	attr.flush();

	if (!attr) {
		throw DBus::ErrorIOError("resetting USB via SCOM driver");
	}
}

uint32_t DisplayHal::BacklightHoursGet()
{
	return read_attr<uint32_t>(attr_path("elapsed_hours"));
}
