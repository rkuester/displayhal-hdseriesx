: ${CPP=g++}

objs="displayhald.o displayhal.o als.o backlight.o buzzer.o keyswitch.o digitaloutput.o"
redo-ifchange $objs
$CPP $LDFLAGS -o $3 $objs $(pkg-config --libs dbus-c++-1)
