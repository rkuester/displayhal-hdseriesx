#!/bin/sh

BIN=$DESTDIR/usr/bin
install --directory $BIN
install displayhalctl $BIN

SBIN=$DESTDIR/usr/sbin
install --directory $SBIN
install displayhald $SBIN

DBUS=$DESTDIR/etc/dbus-1/system.d
install --directory $DBUS
install --mode=644 --no-target-directory dbus.conf $DBUS/displayhal.conf

SYSTEMD=$DESTDIR/lib/systemd/system
install --directory $SYSTEMD
install --mode=644 displayhal.service $SYSTEMD

DBUS=$DESTDIR/usr/share/dbus-1/system-services
install --directory $DBUS
install --mode=644 org.pragmatux.DisplayHal.service $DBUS
