#include "keyswitch.h"
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <ftw.h>
#include <fnmatch.h>
#include <poll.h>
#include <stdexcept>
#include <linux/input.h>

static int return_input_with_sw_lid(const char *path, const struct stat *s, int type)
{
	if (type == FTW_F &&
	    fnmatch("/dev/input/event*", path, FNM_PATHNAME) == 0) {
		uint8_t bits[std::max(EV_MAX, SW_MAX)/8 + 1];
		int fd = open(path, O_RDWR | O_NONBLOCK);
		ioctl(fd, EVIOCGBIT(0, EV_MAX), bits);
		if (bits[EV_SW/8] >> EV_SW%8 & 1) {
			ioctl(fd, EVIOCGBIT(EV_SW, SW_MAX), bits);
			if (bits[SW_LID/8] >> SW_LID%8 & 1)
				return fd;
		}

		close(fd);
	}

	return 0;
}


Keyswitch::Keyswitch(DBus::Connection &c, DBus::DefaultMainLoop *loop)
: DBus::ObjectAdaptor(c, "/org/pragmatux/DisplayHal/Keyswitch")
, fd(ftw("/dev/input", return_input_with_sw_lid, 1))
, watch(fd, POLLIN, loop)
{
	if (fd < 0)
		throw std::runtime_error("keyswitch input not found");

	watch.ready = 
		new DBus::Callback<Keyswitch, void, DBus::DefaultWatch &>(
			this, &Keyswitch::read_input);
}

bool Keyswitch::State()
{
	uint8_t switch_bits[SW_MAX/8+1] = {};
	int rc = ::ioctl(fd, EVIOCGSW(SW_MAX), switch_bits);
	if (rc < 0)
		throw DBus::Error("org.pragmatux.DisplayHal.Error",
			"EVIOCGSW ioctl failed");

	bool lid_closed = (switch_bits[SW_LID/8] >> (SW_LID % 8)) & 1;
	return !lid_closed;
}

void Keyswitch::read_input(DBus::DefaultWatch &watch)
{
	struct ::input_event ev;
	ssize_t n;

	do {
		n = ::read(fd, &ev, sizeof(ev));
		if (n > 0 && ev.type == EV_SW && ev.code == SW_LID) {
			bool lid_closed = ev.value;
			StateChanged(!lid_closed);
		}
	} while (n > 0);
}
