#ifndef DIGITALOUTPUT_H_INCLUDED
#define DIGITALOUTPUT_H_INCLUDED


#include "digitaloutput.adaptor.h"


class DigitalOutput
: public org::pragmatux::DisplayHal::DigitalOutput1_adaptor
, public DBus::ObjectAdaptor
, public DBus::IntrospectableAdaptor
, public DBus::PropertiesAdaptor
{
  public:
	DigitalOutput(DBus::Connection&, const std::string name, const std::string path);

  private:
	void on_set_property(DBus::InterfaceAdaptor &, const std::string &, const DBus::Variant &);
	void set(bool on = true);

	std::string path;
};


#endif // DIGITALOUTPUT_H_INCLUDED
