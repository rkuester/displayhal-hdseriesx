#include <dbus-c++/dbus.h>
#include "displayhal.h"
#include "backlight.h"
#include "buzzer.h"
#include "als.h"
#include "keyswitch.h"
#include "digitaloutput.h"

int main()
{
	DBus::BusDispatcher dispatcher;
	DBus::default_dispatcher = &dispatcher;

	DBus::Connection c = DBus::Connection::SystemBus();
	c.request_name("org.pragmatux.DisplayHal");

	DisplayHal hal(c);
	Backlight backlight(c);
	Buzzer    buzzer(c);
	AmbientLightSensor als(c);
	Keyswitch keyswitch(c, &dispatcher);
	DigitalOutput output0(c, "DigitalOutput0", "/sys/devices/platform/scom.0/hs_out0");
	DigitalOutput output1(c, "DigitalOutput1", "/sys/devices/platform/scom.0/hs_out1");

	DBus::default_dispatcher->enter();
	return 0;
}
