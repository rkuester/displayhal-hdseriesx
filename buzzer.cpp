#include "buzzer.h"
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/kd.h>

Buzzer::Buzzer(DBus::Connection &c)
: DBus::ObjectAdaptor(c, "/org/pragmatux/DisplayHal/Buzzer")
{
}

static void set(bool on)
{
	int fd = ::open("/dev/console", O_WRONLY);
	::ioctl(fd, KIOCSOUND, on);
	::close(fd);
}

void Buzzer::On()
{
	set(true);
}

void Buzzer::Off()
{
	set(false);
}
