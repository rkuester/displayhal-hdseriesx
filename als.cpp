#include "als.h"
#include <string>
#include <fstream>

static const char *object_path =
	"/org/pragmatux/DisplayHal/AmbientLightSensor";
static const std::string illuminance_attribute =
	"/sys/bus/iio/devices/iio:device0/in_illuminance_raw";

AmbientLightSensor::AmbientLightSensor(DBus::Connection &c)
: DBus::ObjectAdaptor(c, object_path)
{
	MaxIlluminanceRaw = 65535;
}

uint32_t AmbientLightSensor::GetIlluminanceRaw()
{
	uint32_t i = 0;

	std::ifstream in;
	in.exceptions(std::ifstream::failbit | std::ifstream::badbit);

	try {
		in.open(illuminance_attribute);
		in >> i;
	}
	catch (...) {
		throw DBus::ErrorIOError("error reading driver attribute");
	}

	return i;
}
