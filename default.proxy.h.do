xml=${1%%.*}.xml

redo-ifchange $xml

if ! OUTPUT=$(dbusxx-xml2cpp $xml --proxy=$3 2>&1)
then
	echo "$OUTPUT" >&2
	exit 1
fi
