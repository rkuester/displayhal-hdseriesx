#include "digitaloutput.h"
#include <fstream>

DigitalOutput::DigitalOutput(DBus::Connection &c, const std::string name, const std::string path)
: DBus::ObjectAdaptor(c, std::string("/org/pragmatux/DisplayHal/") + name)
, path(path)
{
}


void DigitalOutput::
on_set_property(DBus::InterfaceAdaptor &i, const std::string &property, const DBus::Variant &value)
{
	if (property != "Value")
		throw DBus::ErrorInvalidArgs("property not equal to \"Value\"");

	const unsigned v = value;
	switch (v) {
	case 0:
		set(false);
		break;
	case 1:
		set(true);
		break;
	default:
		throw DBus::ErrorInvalidArgs("value must be 0 or 1");
	}
}


void DigitalOutput::set(bool on)
{
	std::ofstream out(path + "/value");
	out << on ? "1" : "0";
}
