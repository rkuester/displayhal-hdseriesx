#ifndef BACKLIGHT_H_INCLUDED
#define BACKLIGHT_H_INCLUDED

#include "backlight.adaptor.h"

class Backlight
: public org::pragmatux::DisplayHal::Backlight1_adaptor
, public DBus::ObjectAdaptor
, public DBus::IntrospectableAdaptor
, public DBus::PropertiesAdaptor
{
  public:
	Backlight(DBus::Connection &c);

  private:
	uint32_t GetBrightness();
	void SetBrightness(const uint32_t&);
};

#endif // BACKLIGHT_H_INCLUDED
