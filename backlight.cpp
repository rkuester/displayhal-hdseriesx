#include "backlight.h"
#include <string>
#include <sys/fcntl.h>
#include <fstream>

static const char *object_path =
	"/org/pragmatux/DisplayHal/Backlight";
static const std::string brightness_attribute =
	"/sys/class/backlight/scom/brightness";

Backlight::Backlight(DBus::Connection &c)
: DBus::ObjectAdaptor(c, object_path)
{
	MaxBrightness = 255;
}

uint32_t Backlight::GetBrightness()
{
	uint32_t b;

	std::ifstream in(brightness_attribute, std::fstream::in);
	in.exceptions(std::ifstream::failbit | std::ifstream::badbit);

	try {
		in >> b;
	}
	catch (...) {
		throw DBus::ErrorIOError("error reading driver attribute");
	}

	return b;
}

void Backlight::SetBrightness(const uint32_t &b)
{
	std::ofstream out(brightness_attribute, std::fstream::out);
	out << b;
}
